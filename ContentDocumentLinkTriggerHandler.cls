public with sharing class ContentDocumentLinkTriggerHandler {
    public static void onAfterInsert(List<ContentDocumentLink> newList){
        
        createFileDocumentOnParentObject(newList);
        
    }
    
    public static void createFileDocumentOnParentObject(List<ContentDocumentLink> newList){
        String childobj1Prefix = Child1__c.getSobjectType().getDescribe().getKeyPrefix();
        String childobj2Prefix = Child2__c.getSobjectType().getDescribe().getKeyPrefix();
        Map<Id,Id> docLinkIdTOContentIdMap = new Map<Id,Id>();
        Map<Id,Set<Id>> parentIdToContentIdMap = new Map<Id,Set<Id>>();
        
        Map<Id,Id> contentDocToLinkIdMap = new Map<Id,Id>();
        Map<Id,Id> linkIdToParentIdMap = new Map<Id,Id>();
        List<ContentDocumentLink> documentLinkList = new List<ContentDocumentLink>();
        for(ContentDocumentLink documentLink : newList){
            if( documentLink.LinkedEntityId != null){
                if(String.valueOf(documentLink.LinkedEntityId).startsWithIgnoreCase(childobj1Prefix)||
                   String.valueOf(documentLink.LinkedEntityId).startsWithIgnoreCase(childobj2Prefix) ){
                       docLinkIdTOContentIdMap.put(documentLink.LinkedEntityId, documentLink.ContentDocumentId);
                       contentDocToLinkIdMap.put(documentLink.ContentDocumentId,documentLink.LinkedEntityId);
                   }
                
            }
        }
        String childobj1_Record_Type_Id = Schema.SObjectType.Child1__c.getRecordTypeInfosByDeveloperName().get('Record_Type_Name').getRecordTypeId();
        for(Child1__c childRec1 : [SELECt id, Parent__c,RecordTypeId FROM Child1__c WHERE id IN: contentDocToLinkIdMap.values()]){
            if(childRec1.RecordTypeId == childobj1_Record_Type_Id){
                if(!linkIdToParentIdMap.containsKey(childRec1.Id)){
                    linkIdToParentIdMap.put(childRec1.Id, childRec1.Parent__c);
                }
            }
        }
        String childRec2ordTypeId =Schema.SObjectType.Child2__c.getRecordTypeInfosByName().get('HSB Hearing').getRecordTypeId();
        for(Child2__c childRec2 : [SELECT id,Hsb_Parent__c,RecordTypeId FROM Child2__c WHERE id IN: contentDocToLinkIdMap.values()]){
            
            if(childRec2.RecordTypeId == childRec2ordTypeId){
                if(!linkIdToParentIdMap.containsKey(childRec2.Id)){
                    linkIdToParentIdMap.put(childRec2.Id, childRec2.Parent__c);
                }
            }
        }
        
        for(ContentDocument doc: [SELECT Id, Title, LatestPublishedVersionId 
                                  FROM ContentDocument 
                                  WHERE id IN: contentDocToLinkIdMap.keySet()]){
                                      
                                      ContentDocumentLink link=new ContentDocumentLink();
                                      link.LinkedEntityId = linkIdToParentIdMap.get(contentDocToLinkIdMap.get(doc.Id));
                                      link.contentdocumentid= doc.Id;
                                      link.ShareType = 'I';
                                      link.Visibility = 'AllUsers';
                                      
                                      documentLinkList.add(link);
                                      
                                  }
        if(documentLinkList != null && !documentLinkList.isEmpty()){
            insert documentLinkList;
        } 
    }
}

